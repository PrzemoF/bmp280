# bmp280
Python software for Raspberry PI for Bosch BMP280 pressure and temperature sensor with SPI/I2C interface as sold by Adafruit. Currently only SPI and limited config options are suppoerted. It uses software emulated SPI, so works on any GPIO pins when properly configured.

Running:

sudo python3 measure.py

For continuous measurement run:

watch -n 1 sudo python3 measure.py

Expected result looks similar to this:

    $ python3 measure.py 
    Temperature: 24.30495 [°C]
    Pressure: 1027.93126 [hPa]
    Temperature: 24.30747 [°C]
    Pressure: 1027.99970 [hPa]
    Temperature: 24.29232 [°C]
    Pressure: 1027.99298 [hPa]
    Temperature: 24.28474 [°C]
    Pressure: 1027.95154 [hPa]
    Temperature: 24.27464 [°C]
    Pressure: 1027.96416 [hPa]
    Temperature: 24.27969 [°C]
    Pressure: 1027.99510 [hPa]
    Temperature: 24.27464 [°C]
    Pressure: 1028.01714 [hPa]
    Temperature: 24.26201 [°C]
    Pressure: 1028.01264 [hPa]
    Temperature: 24.25443 [°C]
    Pressure: 1027.99272 [hPa]
    Temperature: 24.24686 [°C]
    Pressure: 1027.99929 [hPa]
