import RPi.GPIO as GPIO
import time
import numpy


# TODO
#FIXME forced mode might not work properly, use normal mode for now
#FIXME number of bits depend on oversampling, needs to be tested
#FIXME add convinience functions "ultra_low_power", "low_power", "standard_resolution" etc

class bmp280():
    'Class for Bosch BMP280 pressure and temperature sensor with SPI/I2C interfaces as sold by Adafruit. Currently only SPI is supported'

    # BMP280 registers
    BMP280_REG = {
        #@ Calibration data
        'DIG_T1_LSB': 0x88,
        'DIG_T1_MSB': 0x89,
        'DIG_T2_LSB': 0x8A,
        'DIG_T2_MSB': 0x8B,
        'DIG_T3_LSB': 0x8C,
        'DIG_T3_MSB': 0x8D,
        'DIG_P1_LSB': 0x8E,
        'DIG_P1_MSB': 0x8F,
        'DIG_P2_LSB': 0x90,
        'DIG_P2_MSB': 0x91,
        'DIG_P3_LSB': 0x92,
        'DIG_P3_MSB': 0x93,
        'DIG_P4_LSB': 0x94,
        'DIG_P4_MSB': 0x95,
        'DIG_P5_LSB': 0x96,
        'DIG_P5_MSB': 0x97,
        'DIG_P6_LSB': 0x98,
        'DIG_P6_MSB': 0x99,
        'DIG_P7_LSB': 0x9A,
        'DIG_P7_MSB': 0x9B,
        'DIG_P8_LSB': 0x9C,
        'DIG_P8_MSB': 0x9D,
        'DIG_P9_LSB': 0x9E,
        'DIG_P9_MSB': 0x9F,

        #@ Chip ID. Useful to check if communication works
        'ID': 0xD0,

        #@ SOFT_RESET Write only. If set to 0xB6, will perform the same sequence as power on reset.
        'SOFT_RESET': 0xE0,

        #@ STATUS Status of the device, 2 bits only
        # Bit 3 - 1 when conversion is running and 0 when the results have been transferred
        # Bit 0 - 1 when the NVM (Non-Volatile Memory) data are being copied to image registers, 0 when finished
        'STATUS': 0xF3,

        #@ CTRL_MEAS Controls measurements.
        # Bit 7,6,5 temperature oversamplig
        # Bit 4,3,2 pressure oversamplig
        # Bit 1,0   power mode

        'CTRL_MEAS': 0xF4,

        #@ CONFIG
        # Bit 7,6,5 Controls t-standby in normal mode
        # Bit 4,3,2 Controls time constant of the IIR filter
        # Bit 0     Enables 3-wire SPI interface when set to 1
        'CONFIG': 0xF5,

        #@ DATA_START
        # Start of data block in BMP280 memory (the same as PRESS_MSB)
        'DATA_START': 0xF7,

        #@ PRESSURE
        'PRES_MSB': 0xF7,
        'PRES_LSB': 0xF8,
        'PRES_XLSB': 0xF9,

        #@ TEMPERATURE
        'TEMP_MSB': 0xFA,
        'TEMP_LSB': 0xFB,
        'TEMP_XLSB': 0xFC,
    }

    # BMP280 commands
    BMP280_VAL = {
        #@ Chip ID Value fixed to 0x58. Useful to check if communication works
        'ID_VALUE': 0x58,

        # SPI bit to indicate READ or WRITE operation
        'READWRITE': 0x80,

        # Length of data field in bits.
        #@ FIELD_LENGTH
        'FIELD_LENGTH': 24,

        # Number of field in BMP280 memory. First field is pressure, second temperature. Block read is recommended to keep data consistency
        #@ FIELD_NUMBER
        'FIELD_NUMBER': 2,

        # STATUS register bits
        # Bit 3 - 1 when conversion is running and 0 when the results have been transferred
        'STATUS_CONVERSION_RUNNING': 0b1000,
        # Bit 0 - 1 when the NVM (Non-Volatile Memory) data are being copied to image registers, 0 when finished
        'STATUS_COPYING_NVM': 0b0001,

        # Time between measurements in normal mode
        #STANDBY_TIME_N': (0xN, delay in s)
        'STANDBY_TIME_0': (0x0, 0.0005),
        'STANDBY_TIME_1': (0x1, 0.0625),
        'STANDBY_TIME_2': (0x2, 0.125),
        'STANDBY_TIME_3': (0x3, 0.25),
        'STANDBY_TIME_4': (0x4, 0.5),
        'STANDBY_TIME_5': (0x5, 1),
        'STANDBY_TIME_6': (0x6, 2),
        'STANDBY_TIME_7': (0x7, 4),

        # IIR filter coefficient. The BMP280 datasheet doesn't clarify if the values are 0 to 5, so this is the best guess.
        'IIR_FILTER_0': 0x0,
        'IIR_FILTER_2': 0x1,
        'IIR_FILTER_4': 0x2,
        'IIR_FILTER_8': 0x3,
        'IIR_FILTER_16': 0x4,

        # Enables 3-wire SPI interface
        'SPI_3_WIRE_0': 0x0,
        'SPI_3_WIRE_1': 0x1,

        # Oversampling, applied to pressure and temperature
        'OVERSAMPLING_0': 0x0,
        'OVERSAMPLING_1': 0x1,
        'OVERSAMPLING_2': 0x2,
        'OVERSAMPLING_4': 0x3,
        'OVERSAMPLING_8': 0x4,
        'OVERSAMPLING_16': 0x5,

        # Bit shift required to put the value in plac
        'IIR_FILTER_BIT_SHIFT': 5,
        'STANDBY_TIME_BIT_SHIFT': 5,
        'PRES_OVERSAMPLING_BIT_SHIFT': 2,
        'TEMP_OVERSAMPLING_BIT_SHIFT': 5,

        # Power modes
        'POWER_MODE_SLEEP': 0b00,
        'POWER_MODE_FORCED': 0b01,
        'POWER_MODE_NORMAL': 0b11,
    }

    def __init__(self):
        self.temperature = float("nan")
        self.pressure = float("nan")
        self.gpio_configured = False
        self.configure_finished = False

    def _gpio_cleanup(self):
        # GPIO clean up
        GPIO.cleanup(self.SCK)
        GPIO.cleanup(self.CS)
        GPIO.cleanup(self.SDI)
        GPIO.cleanup(self.SDO)

    def _read_byte(self, addr):
        # Reads one byte from addr
        ret = self._spi_read(addr)
        return ret[0]

    def _write_byte(self, addr, value):
        # Write byte of "value" to SPI interface at address "addr"
        self._spi_write(addr, value, 8)

    def _spi_write(self, addr, value, length):
        # Bit banging at address "addr"
        # Write value at addr, "length" is length of value in bits
        spi_addr = addr & (~self.BMP280_VAL['READWRITE'])

        GPIO.output(self.CS, 0)
        time.sleep(self.delay)
        # Write address
        for i in range(8):
            bit = spi_addr & (0x01 << (7 - i))
            if (bit):
                    GPIO.output(self.SDI, 1)
            else:
                    GPIO.output(self.SDI, 0)
            GPIO.output(self.SCK, 0)
            time.sleep(self.delay)
            GPIO.output(self.SCK, 1)
            time.sleep(self.delay)

        # Write data
        for i in range(length):
                bit = value & (0x01 << (length - 1 - i))
                if (bit):
                        GPIO.output(self.SDI, 1)
                else:
                        GPIO.output(self.SDI, 0)
                GPIO.output(self.SCK, 0)
                time.sleep(self.delay)
                GPIO.output(self.SCK, 1)
                time.sleep(self.delay)
        GPIO.output(self.CS, 1)

    def _spi_read(self, addr, block_length=1, chunk_length=8):
        # Bit banging at address "addr"
        # Reads block_length of chunks, each chunk has length in bits chunk_length
        # To read 4 bytes use block_lenght=4, chunk_length=8
        # To read 2 32 bit words use block_lenght=2, chunk_length=32
        ret_value = list()
        spi_addr = addr | self.BMP280_VAL['READWRITE']

        GPIO.output(self.CS, 0)
        time.sleep(self.delay)
        for i in range(8):
            # Write single bit to SPI
            bit = spi_addr & (0x01 << (7 - i))
            if (bit):
                    GPIO.output(self.SDI, 1)
            else:
                    GPIO.output(self.SDI, 0)
            # Flip the SCK
            GPIO.output(self.SCK, 0)
            time.sleep(self.delay)
            GPIO.output(self.SCK, 1)
            time.sleep(self.delay)

        for i in range(block_length):
            chunk_value = 0
            for j in range(chunk_length):
                # Set SCK low
                GPIO.output(self.SCK, 0)
                time.sleep(self.delay)
                # Read bit
                bit = GPIO.input(self.SDO)
                # Set SCK high
                GPIO.output(self.SCK, 1)
                #Put together the read value
                chunk_value = (chunk_value << 1) | bit
                time.sleep(self.delay)
            ret_value.append(chunk_value)
        GPIO.output(self.CS, 1)
        return ret_value

    def _read_calibration_data(self):
        # Read calibration data
        LSB = numpy.uint8(self._read_byte(self.BMP280_REG['DIG_T1_LSB']))
        MSB = numpy.uint8(self._read_byte(self.BMP280_REG['DIG_T1_MSB']))
        self.T1 = numpy.uint16(MSB << 8 | LSB)
        LSB = numpy.uint8(self._read_byte(self.BMP280_REG['DIG_T2_LSB']))
        MSB = numpy.uint8(self._read_byte(self.BMP280_REG['DIG_T2_MSB']))
        self.T2 = numpy.int16(MSB << 8 | LSB)
        LSB = numpy.uint8(self._read_byte(self.BMP280_REG['DIG_T3_LSB']))
        MSB = numpy.uint8(self._read_byte(self.BMP280_REG['DIG_T3_MSB']))
        self.T3 = numpy.int16(MSB << 8 | LSB)
        LSB = numpy.uint8(self._read_byte(self.BMP280_REG['DIG_P1_LSB']))
        MSB = numpy.uint8(self._read_byte(self.BMP280_REG['DIG_P1_MSB']))
        self.P1 = numpy.uint16(MSB << 8 | LSB)
        LSB = numpy.uint8(self._read_byte(self.BMP280_REG['DIG_P2_LSB']))
        MSB = numpy.uint8(self._read_byte(self.BMP280_REG['DIG_P2_MSB']))
        self.P2 = numpy.int16(MSB << 8 | LSB)
        LSB = numpy.uint8(self._read_byte(self.BMP280_REG['DIG_P3_LSB']))
        MSB = numpy.uint8(self._read_byte(self.BMP280_REG['DIG_P3_MSB']))
        self.P3 = numpy.int16(MSB << 8 | LSB)
        LSB = numpy.uint8(self._read_byte(self.BMP280_REG['DIG_P4_LSB']))
        MSB = numpy.uint8(self._read_byte(self.BMP280_REG['DIG_P4_MSB']))
        self.P4 = numpy.int16(MSB << 8 | LSB)
        LSB = numpy.uint8(self._read_byte(self.BMP280_REG['DIG_P5_LSB']))
        MSB = numpy.uint8(self._read_byte(self.BMP280_REG['DIG_P5_MSB']))
        self.P5 = numpy.int16(MSB << 8 | LSB)
        LSB = numpy.uint8(self._read_byte(self.BMP280_REG['DIG_P6_LSB']))
        MSB = numpy.uint8(self._read_byte(self.BMP280_REG['DIG_P6_MSB']))
        self.P6 = numpy.int16(MSB << 8 | LSB)
        LSB = numpy.uint8(self._read_byte(self.BMP280_REG['DIG_P7_LSB']))
        MSB = numpy.uint8(self._read_byte(self.BMP280_REG['DIG_P7_MSB']))
        self.P7 = numpy.int16(MSB << 8 | LSB)
        LSB = numpy.uint8(self._read_byte(self.BMP280_REG['DIG_P8_LSB']))
        MSB = numpy.uint8(self._read_byte(self.BMP280_REG['DIG_P8_MSB']))
        self.P8 = numpy.int16(MSB << 8 | LSB)
        LSB = numpy.uint8(self._read_byte(self.BMP280_REG['DIG_P9_LSB']))
        MSB = numpy.uint8(self._read_byte(self.BMP280_REG['DIG_P9_MSB']))
        self.P9 = numpy.int16(MSB << 8 | LSB)

    def _read_data_block(self):
        # Reads block of data containing raw pressure and temperature measurements
        data = self._spi_read(self.BMP280_REG['DATA_START'], self.BMP280_VAL['FIELD_NUMBER'], self.BMP280_VAL['FIELD_LENGTH'])
        # Last 4 bits are always 0, but are transmitted anyway, strip them
        #FIXME number of bits depend on oversampling
        data[0] = data[0] >> 4
        data[1] = data[1] >> 4
        return data

    def _calculate_pressure(self):
        # Calculate atmospheric pressure in [Pa]
        v1 = self.t_fine / 2 - 64000
        v2 = (v1 * v1 * self.P6) / 32768
        v2 = v2 + v1 * self.P5 * 2
        v2 = v2 / 4 + self.P4 * 65536
        v1 = (self.P3 * v1 * v1 / 524288 + self.P2 * v1) / 524288
        v1 = (1 + v1 / 32768) * self.P1
        p = 1048576 - self._raw_pressure
        p = ((p - v2 / 4096.0) * 6250.0) / v1
        v1 = self.P9 * p * p / 2147483648
        v2 = p * self.P8 / 32768
        self.pressure = p + (v1 + v2 + self.P7) / 16

    def _calculate_temperature(self):
        #Calculate temperature in [degC]. It has to be performed before pressure calculations at least once.
        v1 = (self._raw_temperture / 16384 - self.T1 / 1024) * self.T2
        v2 = ((self._raw_temperture / 131072 - self.T1 / 8192) * (self._raw_temperture / 131072 - self.T1 / 8192)) * self.T3
        self.t_fine = v1 + v2
        self.temperature = self.t_fine / 5120

    def __del__(self):
        self._gpio_cleanup()

    def gpio_setup(self, SCK, SDO, SDI, CS):
        # Setup Raspberry PINS, as numbered on BOARD
        # GPIO for SCK, other name SCLK
        self.SCK = SCK
        # GPIO for SDO, other name MISO
        self.SDO = SDO
        # GPIO for SDI, other name MOSI
        self.SDI = SDI
        # GPIO for CS, other name CE
        self.CS = CS
        # SCK frequency 1 MHz
        self.delay = 1 / 1000000.0

        # GPIO initialisation
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(self.SCK, GPIO.OUT, initial=GPIO.HIGH)
        GPIO.setup(self.CS, GPIO.OUT, initial=GPIO.HIGH)
        GPIO.setup(self.SDI, GPIO.OUT)
        GPIO.setup(self.SDO, GPIO.IN)
        # Check comunication / read ID
        ret = self._read_byte(self.BMP280_REG['ID'])
        if ret != self.BMP280_VAL['ID_VALUE']:
                print("BMP280 returned {} instead of {}. Expect problems.".format(ret, self.BMP280_VAL['ID_VALUE']))

        self._read_calibration_data()
        self.gpio_configured = True

    def configure(self, standby_time=0, iir_filter=0, spi_3_wire=0, pressure_oversampling=16, temperature_oversampling=2, power_mode="NORMAL"):
        # Configures BMP280 stand by time, IIR filter, SPI 3 wire, poressure ovesampling, temperature ovesampling and power mode.
        # If not called by the user directly it will be called with the default settings before performing the forst measurement
        try:
            control_byte = self.BMP280_VAL['IIR_FILTER_' + format(iir_filter)] << self.BMP280_VAL['IIR_FILTER_BIT_SHIFT']
            self.iir_filter = iir_filter
        except KeyError:
            raise Exception('BMP280: Invalid iir_filter: {}. Allowed values: 0 to 4, pleaase refer to BMP280 datasheet or the source code for details'.format(temperature_oversampling))
        try:
            control_byte |= self.BMP280_VAL['STANDBY_TIME_' + format(standby_time)][0] << self.BMP280_VAL['STANDBY_TIME_BIT_SHIFT']
            self.standby_time = standby_time
            self.standby_time_in_s = self.BMP280_VAL['STANDBY_TIME_' + format(standby_time)][1]
        except KeyError:
            raise Exception('BMP280: Invalid standby_time: {}. Allowed values: 0 to 7, refer to BMP280 datasheet or the source code for the details'.format(temperature_oversampling))
        try:
            control_byte |= self.BMP280_VAL['SPI_3_WIRE_' + format(spi_3_wire)]
            self.spi_3_wire = spi_3_wire
        except KeyError:
            raise Exception('BMP280: Invalid spi_3_wire: {}. Allowed values: 0 or 1, refer to BMP280 datasheet or the source code for the details'.format(temperature_oversampling))
        self._write_byte(self.BMP280_REG['CONFIG'], control_byte)

        try:
            control_byte = self.BMP280_VAL['OVERSAMPLING_' + format(temperature_oversampling)] << self.BMP280_VAL['TEMP_OVERSAMPLING_BIT_SHIFT']
            self.temperature_oversampling = temperature_oversampling
        except KeyError:
            raise Exception('BMP280: Invalid temperature oversampling: {}. Allowed values: 0 (skip), 1, 2, 4, 8 and 16'.format(temperature_oversampling))
        try:
            control_byte |= self.BMP280_VAL['OVERSAMPLING_' + format(pressure_oversampling)] << self.BMP280_VAL['PRES_OVERSAMPLING_BIT_SHIFT']
            self.pressure_oversampling = pressure_oversampling
        except KeyError:
            raise Exception('BMP280: Invalid pressure oversampling: {}. Allowed values: 0 (skip), 1, 2, 4, 8 and 16'.format(pressure_oversampling))
        try:
            control_byte |= self.BMP280_VAL['POWER_MODE_' + format(power_mode)]
            self.power_mode = power_mode
        except KeyError:
            raise Exception('BMP280: Invalid power mode: {}. Allowed values: \'SLEEP\', \'FORCED\' and \'NORMAL\''.format(power_mode))
        self._write_byte(self.BMP280_REG['CTRL_MEAS'], control_byte)
        self.configure_finished = True

    def measure(self):
        #Triggers measurement and reads pressure and temperature.
        if not self.gpio_configured:
            raise Exception('BMP280: GPIO not configured, call gpio_setup(SCK=?, SDO=?, SDI=?, CS=?) first!')
        if not self.configure_finished:
            # Perform configure with the default setting if not yet called by the user
            self.configure()
        data = self._read_data_block()
        self._raw_pressure = numpy.int32(data[0])
        self._raw_temperture = numpy.int32(data[1])
        self._calculate_temperature()
        self._calculate_pressure()
